import * as React from 'react';
import BottomSheet, { BottomSheetScrollView } from '@gorhom/bottom-sheet';
import { Image, FlatList, View, Text, Dimensions, StyleSheet, Animated } from 'react-native';

const {width, height} = Dimensions.get('screen');

const ITEM_WIDTH = width;
const ITEM_HEIGHT = height * .75;

const DOT_SIZE = 8
const DOT_SPACEING = 8
const DOT_CONTAINER = DOT_SIZE + DOT_SPACEING

const images = [
    'https://static.zara.net/photos///2020/I/1/1/p/6543/610/091/2/w/2460/6543610091_1_1_1.jpg?ts=1606727905128',
    'https://static.zara.net/photos///2020/I/1/1/p/6543/610/091/2/w/2460/6543610091_2_1_1.jpg?ts=1606727908993',
    'https://static.zara.net/photos///2020/I/1/1/p/6543/610/091/2/w/2460/6543610091_2_2_1.jpg?ts=1606727889015',
    'https://static.zara.net/photos///2020/I/1/1/p/6543/610/091/2/w/2460/6543610091_2_3_1.jpg?ts=1606727896369',
    'https://static.zara.net/photos///2020/I/1/1/p/6543/610/091/2/w/2460/6543610091_2_4_1.jpg?ts=1606727898445',
];

const product = {
    title: 'SOFT MINI CROSSBODY BAG WITH KISS LOCK',
    description: [
        'Mini crossbody bag available in various colours. Featuring two compartments. Handles and detachable crossbody shoulder strap. Lined interior. Clasp with two metal pieces.',
        'Height x Length x Width: 14 x 21.5 x 4.5 cm. / 5.5 x 8.4 x 1.7"'
    ],
    price: '29.99£'
}

export default () => {

    const scrollY = React.useRef(new Animated.Value(0)).current;

    return (
    <View style = {{flex: 1}}>
        <View style = {{height: ITEM_HEIGHT, overflow: "hidden"}}>
            <Animated.FlatList
            showsVerticalScrollIndicator = {false}
            snapToInterval = {ITEM_HEIGHT}
            decelerationRate = "fast"
            onScroll = {Animated.event(
                [{nativeEvent: {contentOffset: {y: scrollY}}}],
                {useNativeDriver: true}
            )}
            data = {images}
            keyExtractor = {(_, index) => index.toString()}
            renderItem = {({item}) => {
                return(
                    <Image 
                        source = {{uri: item}}
                        style = {styles.image}
                    />
                )
            }}
            />
            <View style = {styles.pagination}>
                {images.map((_, index) => {
                    return(
                        <View
                            key = {index}
                            style = {styles.dot}
                        />
                    )
                })}
                <Animated.View
                    style = {[styles.dotContainer, {transform: [{translateY: Animated.divide(scrollY, ITEM_HEIGHT).interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, DOT_CONTAINER]
                    })}]}]}
                />
            </View>
        </View>
        <BottomSheet
            initialSnapIndex = {0}
            snapPoints = {[height - ITEM_HEIGHT, height]}
        >
            <BottomSheetScrollView contentContainerStyle = {{padding: 20}}>
                {product.description.map((_, i) =>
                <View key = {i} style = {{marginBottom: 50}}>    
                    <Text style = {{fontSize: 16, fontWeight: 'bold', marginBottom: 10}}>{product.title}</Text>
                    <Text style = {{fontSize: 14, fontWeight: 'bold', marginBottom: 10}}>{product.price}</Text>
                    <View>
                        {product.description.map((text, index) => {
                            return <Text key = {index} style = {{fontSize: 12, fontWeight: '100', lineHeight: 22}}>{text}</Text>
                        })}
                    </View>
                </View>)}
            </BottomSheetScrollView>
            
        </BottomSheet>
    </View>)
}

const styles = StyleSheet.create({
    image: {
        height: ITEM_HEIGHT,
        width: ITEM_WIDTH,
        resizeMode: "cover"
    },
    pagination: {
        position: "absolute",
        top: ITEM_HEIGHT / 2,
        left: 20
    },
    dot: {
        width: DOT_SIZE,
        height: DOT_SIZE,
        borderRadius: DOT_SIZE,
        marginBottom: DOT_SIZE,
        backgroundColor: "#000"
    },
    dotContainer: {
        width: DOT_CONTAINER ,
        height: DOT_CONTAINER,
        borderRadius: DOT_CONTAINER,
        borderWidth: 1,
        borderColor: "#000",
        position: "absolute",
        top: -DOT_SIZE/2,
        left: -DOT_SIZE/2
    }

})