import React, {useEffect, useRef, useState} from 'react';
import { Dimensions, FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
const { width, height } = Dimensions.get('screen');
const api_key = '563492ad6f9170000100000120d719946fd74681a6b32f8b9bc06b0f'
const API_URL = "https://api.pexels.com/v1/search?query=nature&orientation=portrait&size=small&per_page=20"

const fetchImagesfromPexeles = async ()=> {
  const data = await fetch(API_URL, {
    headers: {
      'Authorization': api_key
    }
  })

  const {photos} = await data.json()

  return photos
}

export default function FlatListSync() {

  const [images, setImages] = useState(null)
  const [activeIndex, setActiveIndex] = useState(0)

  const topRef = useRef()
  const thumRef = useRef()

  const spacing = 10
  const imageSize = 80

  const scrollToActiveIndex = (index) => {
    console.log(index)
    setActiveIndex(index)

    topRef?.current?.scrollToOffset({
      offset: index * width,
      animated: true
    })

    if(index * (imageSize + spacing) - imageSize/2 > width/2)
    {
      thumRef?.current?.scrollToOffset({
        offset: index*(imageSize + spacing) - width/2 + imageSize/2,
        animated: true
      })
    }
    else{
      thumRef?.current?.scrollToOffset({
        offset: 0,
        animated: true
      })
    }
  }

  useEffect(() => {
    
    const fetchImage = async ()=> {
      const images = await fetchImagesfromPexeles()
      setImages(images)
    }

    fetchImage()
  }, [])

  if(!images)
  {
    return <Text>Loading</Text>
  }


  return (
    <View style={{ flex: 1, backgroundColor: '#fff' }}>
      <FlatList
        ref = {topRef}
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator = {false}
        data = {images}
        keyExtractor = {(item) => item.id.toString()}
        onMomentumScrollEnd = {(ev) => scrollToActiveIndex(Math.round(ev.nativeEvent.contentOffset.x / width))}
        renderItem = {({item}) => {
        return <View style = {{width, height}}>
                  <Image
                    source = {{uri: item.src.portrait}}
                    style = {StyleSheet.absoluteFillObject}
                  />
                </View>
        }}
      />

      <FlatList
        ref = {thumRef}
        horizontal
        showsHorizontalScrollIndicator = {false}
        data = {images}
        keyExtractor = {(item) => item.id.toString()}
        style = {{position: "absolute", bottom: imageSize}}
        contentContainerStyle = {{ padding: spacing}}
        renderItem = {({item, index}) => {
        return  <TouchableOpacity
                  onPress = {()=> scrollToActiveIndex(index)}
                >
                  <Image
                      source = {{uri: item.src.portrait}}
                      style = {{height: imageSize, width: imageSize, borderRadius: 12, marginRight: spacing, borderWidth: 2, borderColor: index === activeIndex ? '#fff' : "transparent"}}
                    />
                </TouchableOpacity>
        }}
      />
    </View>
  );
}
