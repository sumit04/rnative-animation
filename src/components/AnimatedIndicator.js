import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { findNodeHandle ,FlatList, StyleSheet, Text, Image, View, Dimensions, Animated, TouchableOpacity } from 'react-native';
const {height, width} = Dimensions.get('screen')

const images = {
  man:
    'https://images.pexels.com/photos/3147528/pexels-photo-3147528.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  women:
    'https://images.pexels.com/photos/2552130/pexels-photo-2552130.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  kids:
    'https://images.pexels.com/photos/5080167/pexels-photo-5080167.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  skullcandy:
    'https://images.pexels.com/photos/5602879/pexels-photo-5602879.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  help:
    'https://images.pexels.com/photos/2552130/pexels-photo-2552130.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
};
const data = Object.keys(images).map((i) => ({
  key: i,
  title: i,
  image: images[i],
  ref: React.createRef()
}));

const Indicator = ({measures, scrollX}) => {

  const inputRange = data.map((_, i) => i * width)
  const widthIndicator = scrollX.interpolate({
    inputRange,
    outputRange: measures.map(i => i.width)
  }) 

  const translateX = scrollX.interpolate({
    inputRange,
    outputRange: measures.map(i => i.x)
  }) 

    return(
        <Animated.View
            style = {{height: 4, backgroundColor: "#fff", width: widthIndicator, position: "absolute", top: 26, transform : [{translateX}], left: 0}}
        />
    )
}

const Tab = ({index, item, onPressTab}) => {
  return(
    <TouchableOpacity onPress = {() => onPressTab(index)}>
      <View key = {`text${index}`} ref = {item.ref}>
        <Text  style = {{color: "#fff", fontSize: 14, textTransform: "uppercase", fontWeight: "bold"}}>{item.title}</Text>
      </View> 
    </TouchableOpacity>
  )
}

const Tabs = ({scrollX, onPressTab}) => {

const [measures, setMeasures] = React.useState([])
const containerRef = React.useRef()

React.useEffect(() => {
    let m = []
    data.map((item, index) => {
        item.ref.current.measureLayout(containerRef.current, (x, y, width, height) => {
            m.push({x, y, width, height})

            if(m.length === data.length)
            {
              setMeasures(m)
            }
        }
        )
    })
}, [])
  return(
    <View style = {{ position: "absolute", width, top: 100}}>
        <View style = {{justifyContent: "space-evenly", flexDirection: "row"}} ref = {containerRef}>
        {data.map((item, index) => {
            return(
                <Tab key = {index} index = {index} item = {item} onPressTab = {onPressTab}/>
            )
        })}

        {measures.length ? <Indicator measures = {measures} scrollX = {scrollX}/> : null}
        </View>
    </View>
  )
}

export default function AnimatedIndicator() {

const scrollX = React.useRef(new Animated.Value(0)).current;

const listRef = React.useRef()

const onPressTab = React.useCallback((itemIndex) => {
  listRef?.current?.scrollToOffset({
    offset: itemIndex * width
  })
})

return (
    <View style={styles.container}>
      <StatusBar hidden />
      <View style = {StyleSheet.absoluteFillObject}>
        <Animated.FlatList
            ref = {listRef}
            onScroll = {Animated.event(
                [{nativeEvent: {contentOffset: {x: scrollX}}}],
                {useNativeDriver: false}
            )}
            horizontal
            pagingEnabled
            showsHorizontalScrollIndicator = {false}
            data = {data}
            keyExtractor = {(item) => item.key.toString()}
            renderItem = {({item, index}) => {
                return(
                    <View>
                        <Image
                            style = {{height, width}}
                            source = {{uri: item.image}}
                        />
                        <View style = {[StyleSheet.absoluteFillObject, {backgroundColor: "rgba(0,0,0,0.3)"}]}/>
                    </View>
                )
            }}
        />
        <Tabs scrollX = {scrollX} onPressTab = {onPressTab}/>
       </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
});