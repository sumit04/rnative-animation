import { StatusBar } from 'expo-status-bar';
import React, {useEffect} from 'react';
import { Animated, StyleSheet, Text, View } from 'react-native';

const ProgressBar = ({step, steps, height}) => {
    const animatedValue = React.useRef(new Animated.Value(0)).current
    const reactive = React.useRef(new Animated.Value(0)).current
    const [width, setWidth] = React.useState(0);
    
    useEffect(() => {
        Animated.timing(animatedValue, {
            toValue: reactive,
            duration: 3000,
            useNativeDriver: true
        }).start()        
    }, [])
    
    useEffect(() => {
        reactive.setValue(-width + (step/steps)*width)
    }, [step, width])

    return(
        <>
            <Text>{step} / {steps}</Text>
            <View
                onLayout = {event => {
                    const newWidth = event.nativeEvent.layout.width
                    console.log(newWidth)
                    setWidth(newWidth)
                }}
                style = {{height, borderRadius: height, backgroundColor: "rgba(0,0,0,0.1)", overflow: "hidden"}}
            >
                <Animated.View style = {{height, borderRadius: height, backgroundColor: "rgba(0,0,0,0.5)", transform: [{translateX: animatedValue}]}}/>
            </View>
        </>
    )
}

export default function AnimatedProgressBar() {
  return (
    <View style={styles.container}>
      <StatusBar hidden />
      <ProgressBar step={2} steps={10} height={20}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    padding: 10
  },
});