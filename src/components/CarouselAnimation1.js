import * as React from 'react';
import { StatusBar, Animated, Text, Image, View, StyleSheet, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
const {width, height} = Dimensions.get('screen');

// https://www.flaticon.com/packs/retro-wave
// inspiration: https://dribbble.com/shots/11164698-Onboarding-screens-animation
// https://twitter.com/mironcatalin/status/1321180191935373312

const bgs = ['#A5BBFF', '#DDBEFE', '#FF63ED', '#B98EFF'];
const DATA = [
  {
    "key": "3571572",
    "title": "Multi-lateral intermediate moratorium",
    "description": "I'll back up the multi-byte XSS matrix, that should feed the SCSI application!",
    "image": "https://image.flaticon.com/icons/png/256/3571/3571572.png"
  },
  {
    "key": "3571747",
    "title": "Automated radical data-warehouse",
    "description": "Use the optical SAS system, then you can navigate the auxiliary alarm!",
    "image": "https://image.flaticon.com/icons/png/256/3571/3571747.png"
  },
  {
    "key": "3571680",
    "title": "Inverse attitude-oriented system engine",
    "description": "The ADP array is down, compress the online sensor so we can input the HTTP panel!",
    "image": "https://image.flaticon.com/icons/png/256/3571/3571680.png"
  },
  {
    "key": "3571603",
    "title": "Monitored global data-warehouse",
    "description": "We need to program the open-source IB interface!",
    "image": "https://image.flaticon.com/icons/png/256/3571/3571603.png"
  }
]

const Indicator = ({scrollX})=> {
    return(
        <View style = {{position: 'absolute', bottom: 70, flexDirection: "row", alignContent: "center"}}>
            {DATA.map((_, index)=> {

                const inputRange = [(index-1) * width, index*width, (index + 1)*width]
                const scale = scrollX.interpolate({
                    inputRange,
                    outputRange: [0.7, 1.7, 0.7],
                    extrapolate: 'clamp'
                })
                const opacity = scrollX.interpolate({
                    inputRange, 
                    outputRange: [0.4, 1, 0.4],
                    extrapolate: "clamp"
                })    
                return(
                    <Animated.View key = {`indicator-${index}`}
                        style = {{
                            width: 10, height: 10, borderRadius: 10, backgroundColor: "#fff", margin: 10, transform: [{scale}], opacity
                        }}
                    />
                )
            })}
        </View>
    )
}

const BackDrop = ({scrollX})=> {

    const backgroundColor = scrollX.interpolate({
        inputRange: bgs.map((_, index) => index * width),
        outputRange: bgs.map((item) => item)
    })
    return(
        <Animated.View
            style = {[StyleSheet.absoluteFillObject, {backgroundColor}]}
        />
    )
}

const Square = ({scrollX}) => {

    const YOLO = Animated.modulo(
        Animated.divide(Animated.modulo(scrollX, width), new Animated.Value(width)), 1
    )

    const rotate = YOLO.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: ["35deg", "0deg", "35deg"]
    })

    const translateX = YOLO.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [0, -height, 0]
    })
    return(
        <Animated.View
            style = {{
                width: height, height, backgroundColor: "#fff", position: "absolute", top: -height*0.6, left: -height* 0.3, borderRadius: 80,
                transform: [{rotate}, {translateX}]
            }}
        />
    )
}

export default function CarouselAnimation1() {

  const scrollX = React.useRef(new Animated.Value(0)).current;

  return (
    <View style={styles.container}>
      <StatusBar hidden />
      <BackDrop scrollX = {scrollX}/>
      <Square scrollX = {scrollX}/>
      <Animated.FlatList
        onScroll = {Animated.event(
            [{nativeEvent: {contentOffset: {x: scrollX}}}],
            {useNativeDriver: false}
        )}
        horizontal
        pagingEnabled
        data = {DATA}
        showsHorizontalScrollIndicator = {false}
        contentContainerStyle = {{marginBottom: 20}}
        keyExtractor = {(item) => item.key}
        renderItem = {({item, index}) => {
            return(
                <View style = {{width, alignItems: "center", padding: 20}}>
                    <View style = {{justifyContent: "center", flex: 0.7}}>
                        <Image
                            source = {{uri: item.image}}
                            style = {{width: width/2, height: height/2, resizeMode: "contain"}}
                        />
                    </View>

                    <View style = {{flex: 0.3}}>
                        <Text style = {{fontSize: 26, fontWeight: "bold", marginBottom: 10, color: "#fff"}}>{item.title}</Text>
                        <Text style = {{fontWeight: '300', color: "#fff"}}>{item.description}</Text>
                    </View>
                </View>
            )
        }}
      />
      <Indicator scrollX = {scrollX}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: "center"
},
});