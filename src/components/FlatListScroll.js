// Inspiration: https://dribbble.com/shots/14154226-Rolodex-Scrolling-Animation/attachments/5780833?mode=media
// Photo by Sharefaith from Pexels
// Background image: https://www.pexels.com/photo/pink-rose-closeup-photography-1231265/


import * as React from 'react';
import { StatusBar, FlatList, Image, Animated, Text, View, Dimensions, StyleSheet, TouchableOpacity, Easing, SafeAreaViewBase, SafeAreaView } from 'react-native';
const { width, height } = Dimensions.get('screen');
import faker from 'faker'
import bg from "../../assets/bg.jpg"

faker.seed(10);
const DATA = [...Array(30).keys()].map((_, i) => {
    return {
        key: faker.random.uuid(),
        image: `https://randomuser.me/api/portraits/${faker.helpers.randomize(['women', 'men'])}/${faker.random.number(60)}.jpg`,
        name: faker.name.findName(),
        jobTitle: faker.name.jobTitle(),
        email: faker.internet.email(),
    };
});

const SPACING = 20;
const AVATAR_SIZE = 70;
const ItemSize = AVATAR_SIZE + SPACING*3;
const background_image = 'https://www.pexels.com/photo/yellow-flower-in-macro-photography-3923589/'

export default () => {

    const scrollY = React.useRef(new Animated.Value(0)).current

    return(
    <View >
        <Image  style = {{...StyleSheet.absoluteFillObject, backgroundColor: "lightpink"}} blurRadius = {810}/>
        <Animated.FlatList
            data = {DATA}
            onScroll = {Animated.event(
                [{nativeEvent: {contentOffset: {y: scrollY }}}],
                {useNativeDriver: true}
            )}
            keyExtractor = {(item)=> item.key}
            contentContainerStyle = {{padding: SPACING, paddingTop: StatusBar.currentHeight + SPACING  || 42}}
            renderItem = {({item, index}) => {

                const inputRange = [
                    -1, 0, ItemSize * index, ItemSize * (index + 2)
                ]

                const scale = scrollY.interpolate({
                    inputRange,
                    outputRange: [1,1,1,0]
                })

                const opacityInputRange = [
                    -1, 0, ItemSize * index, ItemSize * (index + 0.5)
                ]

                const opacity = scrollY.interpolate({
                    inputRange: opacityInputRange,
                    outputRange: [1,1,1,0]
                })

                return(
                    <Animated.View style={{ flexDirection: "row", backgroundColor: 'rgba(255,255,255,0.7)', 
                                    padding: SPACING, marginBottom: SPACING, 
                                    borderRadius: SPACING, 
                                    shadowColor: '#000  ',
                                    shadowOffset: {
                                        width:10,
                                        height: 10
                                    }, shadowOpacity: 3, shadowRadius: 120,
                                    transform: [{scale}],
                                    opacity  
                                }}>
                        <Image
                            source = {{uri: item.image}}
                            style = {{height: AVATAR_SIZE, width: AVATAR_SIZE, borderRadius: AVATAR_SIZE, marginRight: SPACING/2}}
                        />
                        <View>
                            <Text style = {{fontSize: 22, fontWeight: '700'}}>{item.name}</Text>
                            <Text style = {{fontSize: 18, opacity: .7}}>{item.jobTitle}</Text>
                            <Text style = {{fontSize: 12, opacity: .8, color: '#0099cc'}}>{item.email}</Text>
                        </View>
                    </Animated.View>
                )
            }}
        />
    </View>)
}