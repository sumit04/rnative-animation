import React, { useRef, useState } from "react"
import { FlatList, View, StatusBar, Dimensions, PanResponder, Animated, StyleSheet } from "react-native"
import { Card, Text } from "react-native-elements"
import faker from 'faker'
const { width, height } = Dimensions.get('screen');

const SWIPE_THRESHOLD = 0.25 * width;
const SWIPE_OUT_DURATION = 250;

faker.seed(10);
const DATA = [...Array(30).keys()].map((_, i) => {
    return {
        id: faker.random.uuid(),
        uri: `https://randomuser.me/api/portraits/${faker.helpers.randomize(['women', 'men'])}/${faker.random.number(60)}.jpg`,
        text: faker.name.findName(),
    };
});

const FlatListTinderStyle = ()=> {

    const [curIndex, setCurIndex] = useState(0)

    const position = useRef(new Animated.ValueXY()).current;

    const pan = React.useMemo(() => new PanResponder.create({
        onMoveShouldSetPanResponder: () => true,
        onPanResponderMove: (event, gesture) => {
            position.setValue({x: gesture.dx, y: gesture.dy})
        },
        onPanResponderRelease: (event, gesture) => {

            if(gesture.dx > SWIPE_THRESHOLD)
            {
                swipe("right")
            }
            else if(gesture.dx < -SWIPE_THRESHOLD)
            {
                swipe("left")
            }
            else{
                resetComponent()
            }
           
        }

    }));


    const swipe = (direction)=> {
        let x = direction === "right" ? width : -width 

        Animated.timing(position, {
            toValue: {
                x, y:0
            },
            duration: SWIPE_OUT_DURATION,
            useNativeDriver: false
        }).start(()=>{ onSwipeComplete()})
    }

    const onSwipeComplete = ()=> {
        position.setValue({x:0, y:0})
        console.log("in start", curIndex)
        setCurIndex(curIndex + 1)
    }

    const resetComponent = ()=> {
        Animated.spring(position, {
            toValue: {x:0, y:0},
            useNativeDriver: false
        }).start()
    }

    const getStyle = () =>
    {

        const rotate = position.x.interpolate({
            inputRange:[-width*3, 0, width*3],
            outputRange: ['-120deg', '0deg', '120deg']
        })

        return {
            ...position.getLayout(),
            transform: [{rotate}]    
        }
    }
console.log(curIndex)
    return(
        <View> 
            <FlatList
                data = {DATA}
                keyExtractor = {(item) => item.id.toString()}
                showsVerticalScrollIndicator = {false}
                contentContainerStyle = {{paddingTop: StatusBar.currentHeight, height}}
                renderItem = {({item, index}) => {
                    if(index < curIndex) return null

                    if(index === curIndex)
                    return(
                        <Animated.View key = {index} style = {[getStyle(), {zIndex: 99}]} {...pan.panHandlers} >
                            <Card style = {styles.cards}>
                                <Card.Title>{item.text}</Card.Title>
                                <Card.Image source={{uri: item.uri}}>
                                </Card.Image>
                            </Card>
                        </Animated.View>)

                    return(
                        <View key = {index} style = {[styles.cards, {top: 10 * (index - curIndex), zIndex: 5}]}>
                            <Card >
                                <Card.Title>{item.text}</Card.Title>
                                <Card.Image source={{uri: item.uri}}>
                                </Card.Image>
                            </Card>
                        </View>)
                }}/>
        </View>
    )
}

const styles = StyleSheet.create({
    cards:{
        position: "absolute",
        width
    }
})

export default FlatListTinderStyle;