// Inspiration: https://dribbble.com/shots/2343572-Countdown-timer
// 👉 Output of the code: https://twitter.com/mironcatalin/status/1321856493382238208

import * as React from 'react';
import {
  Vibration,
  StatusBar,
  Easing,
  TextInput,
  Dimensions,
  Animated,
  TouchableOpacity,
  FlatList,
  Text,
  View,
  StyleSheet,
} from 'react-native';
const { width, height } = Dimensions.get('window');
const colors = {
  black: '#323F4E',
  red: '#F76A6A',
  text: '#ffffff',
};

const timers = [...Array(13).keys()].map((i) => (i === 0 ? 1 : i * 5));
const ITEM_SIZE = width * 0.38;
const ITEM_SPACING = (width - ITEM_SIZE) / 2;

export default ()=> {

const scrollX = React.useRef(new Animated.Value(0)).current;
const [timer, setTimer] = React.useState(timers[0])
const timerAnimation = React.useRef(new Animated.Value(height)).current;
const buttonAnimation = React.useRef(new Animated.Value(0)).current;
const inputRef = React.useRef();
const textAnimation = React.useRef(new Animated.Value(timers[0])).current;

React.useEffect(() => {
    const listener = textAnimation.addListener(({value}) => {
        inputRef?.current?.setNativeProps({
            text: Math.ceil(value).toString()
        })
    })
    return () => {
        textAnimation.removeListener(listener)
        textAnimation.removeAllListeners(listener)
    }
}, [])

const animation = React.useCallback(() => {
    textAnimation.setValue(timer)
    Animated.sequence([
        Animated.timing(buttonAnimation, {
            toValue: 1,
            duration: 30,
            useNativeDriver: true
        }),
        Animated.timing(timerAnimation, {
            toValue: 0,
            duration: 30,
            useNativeDriver: true
        }),
        Animated.parallel(
            [Animated.timing(timerAnimation, {
            toValue: height,
            duration: timer * 1000,
            useNativeDriver: true
        }),
            Animated.timing(textAnimation, {
                toValue: 0,
                duration: timer * 1000,
                useNativeDriver: true
            })]),
        Animated.delay(400)
    ]).start(()=>{
        Vibration.cancel()
        Vibration.vibrate()
        textAnimation.setValue(timer)
        Animated.timing(buttonAnimation, {
        toValue: 0,
        duration: 300, 
        useNativeDriver: true
    }).start()})
}, [timer])

  return (
    <View style={styles.container}>
      <StatusBar hidden />
      <Animated.View
        style = {[StyleSheet.absoluteFillObject, {
            height, 
            width,
            backgroundColor: colors.red,
            transform: [{translateY: timerAnimation }]
        }]}
      />
      <Animated.View
        style={[
          StyleSheet.absoluteFillObject,
          {
            justifyContent: 'flex-end',
            alignItems: 'center',
            paddingBottom: 100,
          },
        ]}>
        <TouchableOpacity
          onPress={() => {animation()}}
          style = {{transform: [{translateY: buttonAnimation.interpolate({
              inputRange: [0, 1],
              outputRange: [0, 200]
          })}],
          opacity:  buttonAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0]
        })
        }}
          >
          <View
            style={styles.roundButton}
          />
        </TouchableOpacity>
      </Animated.View>
      <View
        style={{
          position: 'absolute',
          top: height / 3,
          left: 0,
          right: 0,
          flex: 1,
        }}>
         <Animated.View style = {[{position: "absolute", width: ITEM_SIZE, justifyContent: "center", alignItems: "center", alignSelf: "center",
                            opacity: buttonAnimation.interpolate({
                                inputRange: [0, 1],
                                outputRange: [0, 1]
                            })}]}>
            <TextInput ref = {inputRef} style = {[styles.text]} defaultValue = {timer.toString()}></TextInput>
          </Animated.View>
          <Animated.FlatList
            onScroll = {Animated.event(
                [{nativeEvent: {contentOffset: {x: scrollX}}}],
                {useNativeDriver: true}
            )}
            onMomentumScrollEnd = {(ev) => {
                const present = Math.round(ev.nativeEvent.contentOffset.x / ITEM_SIZE)
                setTimer(timers[present])
            }}
            horizontal
            snapToInterval = {ITEM_SIZE}
            decelerationRate = "fast"
            showsHorizontalScrollIndicator = {false}
            style = {{flexGrow: 0, opacity: buttonAnimation.interpolate({
                inputRange: [0, 1],
                outputRange: [1, 0]
            })}}
            contentContainerStyle = {{paddingHorizontal: ITEM_SPACING, flexGrow: 0}}
            data = {timers}
            keyExtractor = {(item) => item.toString()}
            renderItem = {({item, index}) => {
                return (
                    <View style = {{width: ITEM_SIZE, justifyContent: "center", alignItems: "center"}}>
                        <Animated.Text style = {[styles.text, 
                        {opacity: scrollX.interpolate({
                            inputRange: [ITEM_SIZE * (index - 1) , ITEM_SIZE * (index), ITEM_SIZE * (index + 1)],
                            outputRange: [0.4, 1, 0.4]
                        }),
                        transform: [{scale: scrollX.interpolate({
                            inputRange: [ITEM_SIZE * (index - 1) , ITEM_SIZE * (index), ITEM_SIZE * (index + 1)],
                            outputRange: [0.7, 1, 0.7]
                        })}]
                        }]}>{item}</Animated.Text>
                    </View>
                )
            }}
          />
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
  },
  roundButton: {
    width: 80,
    height: 80,
    borderRadius: 80,
    backgroundColor: colors.red,
  },
  text: {
    fontSize: ITEM_SIZE * 0.8,
    // fontFamily: 'Menlo',
    color: colors.text,
    fontWeight: '900',
  }
});