import React from 'react';
import {StyleSheet, Text, View } from 'react-native';
import FlatListScroll from './src/components/FlatListScroll';
import FlatListSync from "./src/components/FlatListSync"
import FlatListTinderStyle from './src/components/FlatListTinderStyle';
import CarouselAnimation from './src/components/CarouselAnimation'
import Carousel3d from './src/components/Carousel3d';
import MediumArticle from './src/components/MediumArticle';
import ZaraCrausol from './src/components/ZaraCrausol';
import TimerAnimation from './src/components/TimerAnimation';
import ParallaxCrousal from './src/components/ParallaxCrousal';
import CarouselAnimation1 from './src/components/CarouselAnimation1';
import AnimatedProgressBar from './src/components/AnimatedProgressBar';
import AnimatedIndicator from './src/components/AnimatedIndicator';

export default function App() {

return (
    <View style={{ flex: 1, backgroundColor: '#fff' }}>
      {/* <FlatListSync/> */}
      {/* <FlatListScroll/> */}
      {/* <FlatListTinderStyle/> */}
      {/* <CarouselAnimation/> */}
      {/* <Carousel3d/> */}
      {/* <MediumArticle/> */}
      {/* <ZaraCrausol/> */}
      {/* <TimerAnimation/> */}
      {/* <ParallaxCrousal/> */}
      {/* <CarouselAnimation1/> */}
      {/* <AnimatedProgressBar/> */}
      <AnimatedIndicator/>
    </View>
  );
}

const styles = StyleSheet.create({
});
